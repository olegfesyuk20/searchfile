#include <iostream>
#include "Search.h"


int main() {
    std::string targetFileName; // Replace with the name of the file you are searching for
    fs::path rootDirectory = "/"; // Replace with the path to the root directory
    std::cout << "Enter file's name: ";
    std::cin >> targetFileName;

    if (!fs::exists(rootDirectory)) {
        std::cout << "Root directory does not exist." << std::endl;
        return 1;
    }

    std::vector<fs::path> directories; // Stores subdirectories to be distributed among threads
    std::mutex resultMutex; // Mutex for safe modification of resultPath
    fs::path resultPath; // Stores the found path to the file (if found)

    // Populate the list of directories for distribution among threads
    for (const auto& entry : fs::directory_iterator(rootDirectory)) {
        if (entry.is_directory()) {
            directories.push_back(entry.path());
        }
    }
    //unsigned int * cnt = new unsigned int[static_cast<int>(directories.size())];

    int sizeDir = static_cast<int>(directories.size());
    int numThreads = std::min(sizeDir, THREAD); // Maximum of 8 threads
    int tempCount = 0;

    std::vector<std::thread> threads;
    while(true)
    {
        for (int i = 0; i < numThreads; ++i) {
            threads.emplace_back(searchFiles, std::ref(directories[i + (tempCount * THREAD)]), std::cref(targetFileName), std::ref(resultMutex), std::ref(resultPath));
        }
        // Wait for all threads to finish
        for (auto& thread : threads) {
            thread.join();
        }
        threads.clear();
        if(sizeDir < THREAD)
        {
            break;
        }
        sizeDir -= THREAD;
        tempCount++;
        numThreads = std::min(sizeDir, THREAD);
    }


    if (!resultPath.empty()) {
        std::cout << "File found: " << resultPath << std::endl;
    }
    else {
        std::cout << "File not found." << std::endl;
    }

    return 0;
}