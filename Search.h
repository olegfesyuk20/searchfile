#pragma once
#include <iostream>
#include <string>
#include <filesystem>
#include <thread>
#include <vector>
#include <mutex>
#define THREAD 8

namespace fs = std::filesystem;

fs::path searchFileInDirectory(const fs::path& directory, const std::string& targetFileName);

void searchFiles(fs::path& directory, const std::string& targetFileName, std::mutex& mtx, fs::path& resultPath);