#include "Search.h"

// Function that searches for a file in the specified directory
fs::path searchFileInDirectory(const fs::path& directory, const std::string& targetFileName) {
    try{
        // Check directory accessibility
        if (!fs::exists(directory) || !fs::is_directory(directory)) {
            return ""; // Empty path if the directory does not exist or is not a directory
        }

        // Check directory permissions
        fs::file_status status = fs::status(directory);
        if ((status.permissions() & fs::perms::others_read) == fs::perms::none) {
            return ""; // Empty path if the directory is not readable
        }

        if(fs::is_symlink(directory))return "";


        for (const auto& entry : fs::directory_iterator(directory)) {
            if (fs::is_regular_file(entry) && entry.path().filename() == targetFileName) {
                return entry.path();
            }
            else if(fs::is_directory(entry)){
                fs::path foundPath = searchFileInDirectory(entry, targetFileName);
                if (!foundPath.empty()) {
                    return foundPath;
                }
            }
        }
    } catch (const fs::filesystem_error& ex) {
        //std::cerr << "Filesystem error: " << ex.what() << std::endl;
    }
    return ""; // Empty path if the file is not found
}

// Function that distributes directories among threads to search for the file
void searchFiles(fs::path& directory, const std::string& targetFileName, std::mutex& mtx, fs::path& resultPath) {
    fs::path foundPath = searchFileInDirectory(directory, targetFileName);
    if (!foundPath.empty()) {
        if (resultPath.empty()) {
            mtx.lock(); // Lock the mutex before changing resultPath
            resultPath = foundPath;
            mtx.unlock(); // Unlock the mutex after changing resultPath
        }
    }
}